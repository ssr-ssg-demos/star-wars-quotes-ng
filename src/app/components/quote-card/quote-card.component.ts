import { Component, OnInit, Input } from '@angular/core';
import { Quote } from 'src/types';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-quote-card',
  templateUrl: './quote-card.component.html',
  styleUrls: ['./quote-card.component.scss'],
})
export class QuoteCardComponent implements OnInit {
  @Input() quote: Quote;

  constructor(
    private apollo: Apollo,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'arrow-up',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/keyboard_arrow_up.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'arrow-down',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/keyboard_arrow_down.svg'
      )
    );
  }

  ngOnInit(): void {}

  upvote() {
    this.quote.votes++; // react immediately to user input
    this.apollo
      .mutate({
        mutation: gql`
          mutation Upvote($id: Int!) {
            update_quote(where: { id: { _eq: $id } }, _inc: { votes: 1 }) {
              returning {
                id
                votes
              }
            }
          }
        `,
        variables: {
          id: this.quote.id,
        },
      })
      .subscribe();
  }

  downvote() {
    this.quote.votes--;
    this.apollo
      .mutate({
        mutation: gql`
          mutation Upvote($id: Int!) {
            update_quote(where: { id: { _eq: $id } }, _inc: { votes: -1 }) {
              returning {
                id
                votes
              }
            }
          }
        `,
        variables: {
          id: this.quote.id,
        },
      })
      .subscribe();
  }
}
