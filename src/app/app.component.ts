import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { Quote, Query_Root } from 'src/types';
import gql from 'graphql-tag';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Star Wars Quotes';
  episodeSelections = [
    'All',
    'Episode IV | A New Hope',
    'Episode V | The Empire Strikes Back',
    'Episode VI | The Return of the Jedi',
    'Episode I | The Phantom Menace',
    'Episode II | Attack of the Clones',
    'Episode III | Revenge of the Sith',
    'Star Wars The Clone Wars',
    'Star Wars Rebels',
    'Episode VII | The Force Awakens',
    'Rogue One: A Star Wars Story',
  ];
  selectedEpisode: string;
  allQuotes: Quote[] = [];
  filteredQuotes: Quote[] = [];
  loading = true;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.apollo
      .watchQuery({
        query: gql`
          query GetQuotes {
            quote(order_by: { votes: desc }) {
              id
              quote
              character
              votes
              episode
            }
          }
        `,
      })
      .valueChanges.subscribe((result: ApolloQueryResult<Query_Root>) => {
        this.loading = false;
        this.allQuotes = result.data.quote;
        // this.filteredQuotes = result.data.quote;
        this.filterQuotes();
      });
  }

  filterQuotes() {
    if (!this.selectedEpisode || this.selectedEpisode === 'All') {
      this.filteredQuotes = this.allQuotes;
    } else {
      this.filteredQuotes = this.allQuotes.filter(
        (quote) => quote.episode === this.selectedEpisode
      );
    }
  }
}
