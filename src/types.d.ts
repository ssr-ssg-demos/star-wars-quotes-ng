export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** mutation root */
export type Mutation_Root = {
   __typename?: 'mutation_root';
  /** delete data from the table: "quote" */
  delete_quote?: Maybe<Quote_Mutation_Response>;
  /** insert data into the table: "quote" */
  insert_quote?: Maybe<Quote_Mutation_Response>;
  /** update data of the table: "quote" */
  update_quote?: Maybe<Quote_Mutation_Response>;
};


/** mutation root */
export type Mutation_RootDelete_QuoteArgs = {
  where: Quote_Bool_Exp;
};


/** mutation root */
export type Mutation_RootInsert_QuoteArgs = {
  objects: Array<Quote_Insert_Input>;
  on_conflict?: Maybe<Quote_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_QuoteArgs = {
  _inc?: Maybe<Quote_Inc_Input>;
  _set?: Maybe<Quote_Set_Input>;
  where: Quote_Bool_Exp;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** query root */
export type Query_Root = {
   __typename?: 'query_root';
  /** fetch data from the table: "quote" */
  quote: Array<Quote>;
  /** fetch aggregated fields from the table: "quote" */
  quote_aggregate: Quote_Aggregate;
  /** fetch data from the table: "quote" using primary key columns */
  quote_by_pk?: Maybe<Quote>;
};


/** query root */
export type Query_RootQuoteArgs = {
  distinct_on?: Maybe<Array<Quote_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Quote_Order_By>>;
  where?: Maybe<Quote_Bool_Exp>;
};


/** query root */
export type Query_RootQuote_AggregateArgs = {
  distinct_on?: Maybe<Array<Quote_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Quote_Order_By>>;
  where?: Maybe<Quote_Bool_Exp>;
};


/** query root */
export type Query_RootQuote_By_PkArgs = {
  id: Scalars['Int'];
};

/** columns and relationships of "quote" */
export type Quote = {
   __typename?: 'quote';
  character: Scalars['String'];
  episode: Scalars['String'];
  id: Scalars['Int'];
  quote: Scalars['String'];
  votes: Scalars['Int'];
};

/** aggregated selection of "quote" */
export type Quote_Aggregate = {
   __typename?: 'quote_aggregate';
  aggregate?: Maybe<Quote_Aggregate_Fields>;
  nodes: Array<Quote>;
};

/** aggregate fields of "quote" */
export type Quote_Aggregate_Fields = {
   __typename?: 'quote_aggregate_fields';
  avg?: Maybe<Quote_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Quote_Max_Fields>;
  min?: Maybe<Quote_Min_Fields>;
  stddev?: Maybe<Quote_Stddev_Fields>;
  stddev_pop?: Maybe<Quote_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Quote_Stddev_Samp_Fields>;
  sum?: Maybe<Quote_Sum_Fields>;
  var_pop?: Maybe<Quote_Var_Pop_Fields>;
  var_samp?: Maybe<Quote_Var_Samp_Fields>;
  variance?: Maybe<Quote_Variance_Fields>;
};


/** aggregate fields of "quote" */
export type Quote_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Quote_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "quote" */
export type Quote_Aggregate_Order_By = {
  avg?: Maybe<Quote_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Quote_Max_Order_By>;
  min?: Maybe<Quote_Min_Order_By>;
  stddev?: Maybe<Quote_Stddev_Order_By>;
  stddev_pop?: Maybe<Quote_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Quote_Stddev_Samp_Order_By>;
  sum?: Maybe<Quote_Sum_Order_By>;
  var_pop?: Maybe<Quote_Var_Pop_Order_By>;
  var_samp?: Maybe<Quote_Var_Samp_Order_By>;
  variance?: Maybe<Quote_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "quote" */
export type Quote_Arr_Rel_Insert_Input = {
  data: Array<Quote_Insert_Input>;
  on_conflict?: Maybe<Quote_On_Conflict>;
};

/** aggregate avg on columns */
export type Quote_Avg_Fields = {
   __typename?: 'quote_avg_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "quote" */
export type Quote_Avg_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "quote". All fields are combined with a logical 'AND'. */
export type Quote_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Quote_Bool_Exp>>>;
  _not?: Maybe<Quote_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Quote_Bool_Exp>>>;
  character?: Maybe<String_Comparison_Exp>;
  episode?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  quote?: Maybe<String_Comparison_Exp>;
  votes?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "quote" */
export enum Quote_Constraint {
  /** unique or primary key constraint */
  QuotePkey = 'quote_pkey'
}

/** input type for incrementing integer columne in table "quote" */
export type Quote_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  votes?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "quote" */
export type Quote_Insert_Input = {
  character?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  quote?: Maybe<Scalars['String']>;
  votes?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Quote_Max_Fields = {
   __typename?: 'quote_max_fields';
  character?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  quote?: Maybe<Scalars['String']>;
  votes?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "quote" */
export type Quote_Max_Order_By = {
  character?: Maybe<Order_By>;
  episode?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  quote?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Quote_Min_Fields = {
   __typename?: 'quote_min_fields';
  character?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  quote?: Maybe<Scalars['String']>;
  votes?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "quote" */
export type Quote_Min_Order_By = {
  character?: Maybe<Order_By>;
  episode?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  quote?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** response of any mutation on the table "quote" */
export type Quote_Mutation_Response = {
   __typename?: 'quote_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Quote>;
};

/** input type for inserting object relation for remote table "quote" */
export type Quote_Obj_Rel_Insert_Input = {
  data: Quote_Insert_Input;
  on_conflict?: Maybe<Quote_On_Conflict>;
};

/** on conflict condition type for table "quote" */
export type Quote_On_Conflict = {
  constraint: Quote_Constraint;
  update_columns: Array<Quote_Update_Column>;
  where?: Maybe<Quote_Bool_Exp>;
};

/** ordering options when selecting data from "quote" */
export type Quote_Order_By = {
  character?: Maybe<Order_By>;
  episode?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  quote?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** select columns of table "quote" */
export enum Quote_Select_Column {
  /** column name */
  Character = 'character',
  /** column name */
  Episode = 'episode',
  /** column name */
  Id = 'id',
  /** column name */
  Quote = 'quote',
  /** column name */
  Votes = 'votes'
}

/** input type for updating data in table "quote" */
export type Quote_Set_Input = {
  character?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  quote?: Maybe<Scalars['String']>;
  votes?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Quote_Stddev_Fields = {
   __typename?: 'quote_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "quote" */
export type Quote_Stddev_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Quote_Stddev_Pop_Fields = {
   __typename?: 'quote_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "quote" */
export type Quote_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Quote_Stddev_Samp_Fields = {
   __typename?: 'quote_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "quote" */
export type Quote_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Quote_Sum_Fields = {
   __typename?: 'quote_sum_fields';
  id?: Maybe<Scalars['Int']>;
  votes?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "quote" */
export type Quote_Sum_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** update columns of table "quote" */
export enum Quote_Update_Column {
  /** column name */
  Character = 'character',
  /** column name */
  Episode = 'episode',
  /** column name */
  Id = 'id',
  /** column name */
  Quote = 'quote',
  /** column name */
  Votes = 'votes'
}

/** aggregate var_pop on columns */
export type Quote_Var_Pop_Fields = {
   __typename?: 'quote_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "quote" */
export type Quote_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Quote_Var_Samp_Fields = {
   __typename?: 'quote_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "quote" */
export type Quote_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Quote_Variance_Fields = {
   __typename?: 'quote_variance_fields';
  id?: Maybe<Scalars['Float']>;
  votes?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "quote" */
export type Quote_Variance_Order_By = {
  id?: Maybe<Order_By>;
  votes?: Maybe<Order_By>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/** subscription root */
export type Subscription_Root = {
   __typename?: 'subscription_root';
  /** fetch data from the table: "quote" */
  quote: Array<Quote>;
  /** fetch aggregated fields from the table: "quote" */
  quote_aggregate: Quote_Aggregate;
  /** fetch data from the table: "quote" using primary key columns */
  quote_by_pk?: Maybe<Quote>;
};


/** subscription root */
export type Subscription_RootQuoteArgs = {
  distinct_on?: Maybe<Array<Quote_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Quote_Order_By>>;
  where?: Maybe<Quote_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootQuote_AggregateArgs = {
  distinct_on?: Maybe<Array<Quote_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Quote_Order_By>>;
  where?: Maybe<Quote_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootQuote_By_PkArgs = {
  id: Scalars['Int'];
};
